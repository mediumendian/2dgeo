public class Vector {

	private double[] components;

	final public int DIMENSION;

	public Vector(double[] components) {
		this.components = components;
		DIMENSION = components.length;
	}

	public void printComponents() {
		System.out.println("Components of vector: ");
		for (double d : components) {
			System.out.println(d);
		}
	}

	public double[] getComponents() {
		return components;
	}

	public double scalarProduct(Vector v2) {
		double sum = 0;
		//if (this.DIMENSION == v2.DIMENSION) {
			for (int i = 0; i < this.DIMENSION; i++) {
				sum += this.getComponents()[i] * v2.getComponents()[i];
			}
			return sum;

		//} else {
			//return 0;
		}
	//}
	public double abs() {
		double sum = 0;
		for(double d: this.components) {
			sum += d*d;
		}
		return Math.sqrt(sum);
	}
	
	
}
