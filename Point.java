public class Point {

    public String name;

    double x, y;

    public Point(String name, double x, double y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return this.name;
    }

    public void printName() {
        System.out.println("Point " + this.name);
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double[] getCoords() {
    	double[] coords = new double[2];
        coords[0] = this.x;
        coords[1] = this.y;
        return coords;
    }
    
    public double distanceToRoot() {
    	return this.calculateDistance(Geometry.ROOT);
    }

    public double calculateDistance(Point p2) {
        return Math.sqrt(Math.pow((this.x - p2.getX()), 2)
                + Math.pow(this.y - p2.getY(), 2));
    }

    public String printDistance(Point p2) {
        double distance = this.calculateDistance(p2);
        String distanceMessage = ("Distance between points " + this.name + " and " + p2.name + ": " + distance);
        System.out.println(distanceMessage);
        return distanceMessage;

    }

}
