public class Geometry {

	final static Point ROOT = new Point("0", 0, 0);

	public static void print2dCoords(double[] coords) {
		System.out.println("X: " + coords[0]);
		System.out.println("Y: " + coords[1]);
	}

	public static void printPoints(Point[] points) {
		for (Point p : points) {
			print2dCoords(p.getCoords());
		}
	}

	public static void main(String[] args) {
		Point p1, p2, p3, p4;
		p1 = new Point("a", 2, 3);
		p2 = new Point("b", 2, 1);

		p1.printName();
		print2dCoords(p1.getCoords());
		System.out.println("Distance to root: " + p1.distanceToRoot());

		System.out.println("\n");

		p2.printName();
		print2dCoords(p2.getCoords());
		p2.printDistance(p1);

		System.out.println("\n");

		Line l1;
		Line l2;

		p1 = new Point("a", 0, 0);
		p2 = new Point("b", 1, 1);
		p3 = new Point("c", 0, 0);
		p4 = new Point("d", -1, 0);

		l1 = new Line("AB", p1, p2);
		l2 = new Line("BC", p3, p4);

		l1.printName();
		l1.printPoints();
		l1.printLength();

		System.out.println('\n');

		l2.printName();
		l2.printPoints();
		l2.printLength();

		double[] abbc = l1.intersectionCoordinate(l2);
		System.out.println("Intersection of " + l1.getName() + " and "
				+ l2.getName());
		print2dCoords(abbc);

		System.out.println("Slope of line " + l1.getName() + ": "
				+ l1.calculateSlope());

		System.out.println("Y-axis intercept: " + l1.calculateYICPT());
		System.out.println("X-axis intercept: " + l1.calculateXICPT());

		System.out.println("Formula of " + l1.getName() + ": "
				+ l1.getLineFormula());

		System.out.println("Line " + l1.getName() + " is inclined "
				+ l1.calculateAngle() + " degrees to the x-axis");

		System.out.println("\n");

		Triangle t1 = new Triangle("BCA", 0, 0, 1, 1, 2, 0);
		t1.printName();
		Line[] lines = t1.lines();
		System.out.println("Angles between lines of t1:");
		System.out.println(lines[0].angleToLine(lines[1]));
		System.out.println(lines[1].angleToLine(lines[2]));
		System.out.println(lines[2].angleToLine(lines[0]));
		

		printPoints(t1.points());

		System.out.println("Area of triangle " + t1.getName() + ": "
				+ t1.area());

		// System.out.println(t1.intersectionInTriangle(l2));

		double[] components1 = new double[2];
		components1[0] = 0;
		components1[1] = 4;

		double[] components2 = new double[2];
		components2[0] = 2;
		components2[1] = 0;

		Vector v1 = new Vector(components1);
		v1.printComponents();

		Vector v2 = new Vector(components2);
		v2.printComponents();

		System.out.println("Scalar product: " + v1.scalarProduct(v2));

		Line ab = new Line("AB", p1, p2);
		Vector test = ab.toVector();
		System.out.println("Components of test vector: ");
		test.printComponents();

		System.out.println("Absolute value of test: " + v1.abs());
		
		System.out.println("Angle of l1 to l2: " + l1.angleToLine(l2));
		
		//somethings wrong here...
		System.out.println(t1.pointInSector(new Point("", -0.5, 1)));

	}
}
