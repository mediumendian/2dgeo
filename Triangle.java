public class Triangle {

	private Point[] points = new Point[3];
	private Line[] lines = new Line[3];

	private String name;

	public Triangle(String name, double x1, double y1, double x2, double y2,
			double x3, double y3) {
		this.name = name;

		points[0] = new Point("a", x1, y1);
		points[1] = new Point("b", x2, y2);
		points[2] = new Point("c", x3, y3);

		lines[0] = new Line("ab", points[0], points[1]);
		lines[1] = new Line("bc", points[1], points[2]);
		lines[2] = new Line("ac", points[0], points[2]);

	}

	public void printName() {
		System.out.println("Triangle " + name);
	}

	public String getName() {
		return this.name;
	}

	public double getPOneX() {
		return points[0].getX();
	}

	public double getPOneY() {
		return points[0].getY();
	}

	public double getPTwoX() {
		return points[1].getX();
	}

	public double getPTwoY() {
		return points[1].getY();
	}

	public double getPThreeX() {
		return points[2].getX();
	}

	public double getPThreeY() {
		return points[2].getY();
	}

	public Point[] points() {
		return points;
	}
	
	public Line[] lines() {
		return lines;
	}

	public double area() {
		// half the sum of the lengths of the sides of the triangle.
		double s = (lines[0].length() + lines[1].length() + lines[2].length()) / 2;
		return Math.sqrt(s * (s - lines[0].length()) * (s - lines[1].length())
				* (s - lines[2].length()));
	}

	public boolean pointInSector(Point x) {
		double linesAngle = lines[0].angleToLine(lines[1]);
		Line t = new Line(lines[0].getFirstX(), lines[0].getFirstY(), x.getX(), x.getY());
		double pointAngle = lines[0].angleToLine(t);
		System.out.println(linesAngle);
		System.out.println(pointAngle);
		return pointAngle<linesAngle;
	}

	/*
	 * public boolean intersectionInTriangle(Line l1) { boolean
	 * intersectionInTriangle = false; for (Line l : lines) {
	 * System.out.println(l.getLineFormula()); if (l.intersection(l1)) {
	 * intersectionInTriangle = (l.intersectionCoordinateX(l1) > l.getFirstX()
	 * && l.intersectionCoordinateX(l1) < l.getLastX()) ||
	 * (l.intersectionCoordinateX(l1) < l.getFirstX() &&
	 * l.intersectionCoordinateX(l1) > l.getLastX());
	 * 
	 * } else { intersectionInTriangle = false; } } return
	 * intersectionInTriangle; }
	 */

}

// TODO pointInSector