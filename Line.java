public class Line {

    private Point p1;
    private Point p2;
    private String name;

    public Line(String name, Point p1, Point p2) {
        this.name = name;
        this.p1 = p1;
        this.p2 = p2;
    }
    
    public Line(double x1, double y1, double x2, double y2) {
    	Point p1, p2;
    	this.p1 = new Point("", x1, y1);
    	this.p2 = new Point("", x2, y2);    	
    }

    public void printName() {
        System.out.println("Line " + name);
    }
    
    public String getName() {
    	return this.name;
    }

    public void setFirst(double x, double y) {
        p1.x = x;
        p1.y = y;
    }

    public void setLast(double x, double y) {
        p2.x = x;
        p2.y = y;
    }

    public double getFirstX() {
        return p1.getX();
    }

    public double getFirstY() {
        return p1.getY();
    }

    public double getLastX() {
        return p2.getX();
    }

    public double getLastY() {
        return p2.getY();
    }

    public void printPoints() {
        System.out.println("P1: \n" + "x: " + p1.getX() + "\ny: " + p1.getY()
                + "\nP2:\n" + "x: " + p2.getX() + "\ny: " + p2.getY() + "\n");
    }

    public double length() {
        return p1.calculateDistance(p2);
    }

    public String printLength() {
        double length = this.length();
        String lengthMessage = "The line " + this.name + " is " + length + " long.";
        System.out.println(lengthMessage);
        return lengthMessage;
    }

    
    public boolean between(Line a, Line b) {
    	return false;
    }

    public double calculateSlope() {
        return ((p1.getY() - p2.getY()) / (p1.getX() - p2.getX()));
    }

    public double calculateYICPT() {
        double m = this.calculateSlope();
        return (-m * p1.getX()) + p1.getY();
    }

    public String getLineFormula() {
        return "y = f(x) = " + this.calculateSlope() + "x + "
                + this.calculateYICPT();
    }
    
    public Vector toVector() {
    	double[] components = new double[2];
    	components[0] = this.getLastX()-this.getFirstX();
    	components[1] = this.getLastY()-this.getFirstY();
    	Vector t = new Vector(components);
    	return t;
    }

    public double calculateX(double x) {
        return x * this.calculateSlope() + this.calculateYICPT();
    }

    public double calculateXICPT() {
        double m = this.calculateSlope();
        double n = this.calculateYICPT();
        return -(n / m);
    }

    public boolean intersection(Line l) {
        return this.calculateSlope() != l.calculateSlope();
    }

    public double intersectionCoordinateX(Line l2) {
        double m1, m2;
        double n1, n2;

        m1 = this.calculateSlope();
        m2 = l2.calculateSlope();

        n1 = this.calculateYICPT();
        n2 = l2.calculateYICPT();
        return (((n2 - n1) / (m1 - m2)));
    }

    public double[] intersectionCoordinate(Line l2) {
        double m1, m2;
        double n1, n2;

        m1 = this.calculateSlope();
        m2 = l2.calculateSlope();

        n1 = this.calculateYICPT();
        n2 = l2.calculateYICPT();

        double intersectionX = (((n2 - n1) / (m1 - m2)));

        double intersectionY = (((m1 * n2) - (m2 * n1)) / (m1 - m2));
        
        double[] isct = new double[2];
        
        isct[0] = intersectionX;
        isct[1] = intersectionY;
        
        return isct;
    }

    public double calculateAngle() {
        return Math.toDegrees(Math.atan(this.calculateSlope()));
    }
    
    public double angleToLine(Line x) {
    	Vector v1 = this.toVector();
    	Vector v2 = x.toVector();
    	double angle = Math.toDegrees(Math.acos(v1.scalarProduct(v2)/(v1.abs()*v2.abs())));
    	return angle;
    }
}
