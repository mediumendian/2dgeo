2dgeo
=====

Basic 2-dimensional calculations with points, lines and triangles. Example output:

Point a
X: 2.0
Y: 3.0

Distance to root: 3.605551275463989


Point b
X: 2.0
Y: 1.0

Distance between points b and a: 2.0


Line AB
P1: 
x: 0.0
y: 0.0
P2:
x: 2.0
y: 1.0

The line AB is 2.23606797749979 long.


Line BC
P1: 
x: 3.0
y: 5.0
P2:
x: 4.0
y: 6.0

The line BC is 1.4142135623730951 long.

Intersection of AB and BC
X: -4.0
Y: -2.0

Slope of line AB: 0.5

Y-axis intercept: 0.0

X-axis intercept: -0.0

Formula of AB: y = f(x) = 0.5x + 0.0

Line AB is inclined 26.56505117707799 degrees to the x-axis


Triangle BCA

X: 0.0
Y: 0.0

X: 0.0
Y: 1.0

X: 2.0
Y: 0.0

Area of triangle BCA: 1.0

y = f(x) = 1.0x + 2.0
